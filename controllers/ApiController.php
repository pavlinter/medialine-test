<?php

namespace app\controllers;

use app\models\Category;
use app\models\News;
use app\modules\appadm\models\NewsCategory;
use Yii;
use yii\data\Pagination;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Api Controller
 */
class ApiController extends Controller
{

    /**
     * @return string|\yii\web\Response
     */
    public function actionNews()
    {
        $cat_id = Yii::$app->request->get('cat_id');
        $deep = Yii::$app->request->get('deep', 3);

        $ids = Category::getChildIds($cat_id , $deep);

        $query = new Query();

        $query->select('n.id, n.title, n.text, n.created_at')
            ->innerJoin(['nc' => NewsCategory::tableName()], 'nc.news_id=n.id')
            ->from(['n' => News::tableName()])->indexBy('id')
            ->andWhere(['nc.category_id' =>  $ids,]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 20, //для примера можно поставить 2
        ]);

        $rows = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $json = [
            'pages' => $pages->getLinks(true),
            'news' => $rows
        ];
        return $this->output($json);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCategory()
    {
        $deep = Yii::$app->request->get('deep', 3);
        $categories = Category::getCategories($deep);

        $json = [
            'categories' => $categories
        ];
        return $this->output($json);
    }


    /**
     * @param $json
     * @return string|void
     */
    public function output($json)
    {
        if (Yii::$app->request->get('print')) {
            return $this->render('print', [
                'json' => $json,
            ]);
        }

        return Json::encode($json);
    }

}
