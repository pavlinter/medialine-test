<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 *
 */
class Category extends \app\modules\appadm\models\Category
{
    /**
     * @param $parent_id
     * @param int $maxDeep
     * @return array
     */
    public static function getChildIds($parent_id, $maxDeep = 3)
    {
        //зависит от ситуации, возможно нужно было сразу все рубрики вытащить в кешь, если мало рубрик.
        $key = 'getChildIds:' . $parent_id . ':' . $maxDeep;
        $ids = Yii::$app->cache->get($key);
        if ($ids === false) {
            $ids = [$parent_id];
            $level = 1;
            static::recursive_child_ids($ids, $level, $parent_id, $maxDeep);

            $query = new \yii\db\Query();
            $sql = $query->select('MAX(updated_at)')
                ->from(Category::tableName())
                ->createCommand()
                ->getRawSql();

            $seconds = 86400; //86400
            Yii::$app->cache->set($key, $ids, $seconds, new \yii\caching\DbDependency([
                'sql' => $sql,
            ]));

        }

        return $ids;
    }

    /**
     * @param $ids
     * @param $level
     * @param $parent_id
     * @param $maxDeep
     * @return bool
     */
    public static function recursive_child_ids(&$ids, $level, $parent_id, $maxDeep)
    {
        if ($level >= $maxDeep) {
            return false;
        }
        $level++;
        $query = (new Query())->select("id")->from(Category::tableName())->where(['id_parent' => $parent_id]);

        foreach ($query->each() as $row) {
            $ids[] = $row['id'];
            static::recursive_child_ids($ids, $level, $row['id'], $maxDeep);
        }
    }

    /**
     * @param int $maxDeep
     * @param null $id
     * @return array
     */
    public static function getCategories($maxDeep = 3, $id = null)
    {
        //если категорий будет не много, то возможно было лучше достать все категории сразу и потом их отсортеровать id_parent
        $key = 'getCategories:' . $maxDeep . ':' . $id;
        $rows = Yii::$app->cache->get($key);
        if ($rows === false) {

            $level = 1;
            $query = new Query();
            $query->select('id, title')
                ->from(Category::tableName())
                ->andWhere(['id_parent' =>  $id]);

            $rows = $query->all();
            foreach ($rows as $i => $row) {
                $rows[$i]['children'] = static::recursive_categories($level, $row['id'], $maxDeep);
            }

            $query = new \yii\db\Query();
            $sql = $query->select('MAX(updated_at)')
                ->from(Category::tableName())
                ->createCommand()
                ->getRawSql();

            $seconds = 86400; //86400
            Yii::$app->cache->set($key, $rows, $seconds, new \yii\caching\DbDependency([
                'sql' => $sql,
            ]));

        }

        return $rows;
    }

    /**
     * @param $level
     * @param $id
     * @param $maxDeep
     * @return array
     */
    public static function recursive_categories($level, $id, $maxDeep)
    {
        if ($level >= $maxDeep) {
            return [];
        }
        $level++;

        $query = (new Query())->select('id, title')
            ->from(Category::tableName())
            ->andWhere(['id_parent' =>  $id]);

        $rows = $query->all();
        foreach ($rows as $i => $row) {
            $rows[$i]['children'] = static::recursive_categories($level, $row['id'], $maxDeep);
        }
        return $rows;
    }
}
