<?php
use app\helpers\Url;
use app\modules\icheck\widgets\Checkbox;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
$this->context->layout = '/base';
$appAsset = \app\assets_b\AppAsset::register($this);
?>

<a href="<?= Url::current(['print' => null]) ?>" class="btn btn-primary">json</a>
<div>

    <?php
    \yii\helpers\VarDumper::dump($json, 10, true);
    ?>
</div>