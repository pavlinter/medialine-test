<?php

use app\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\core\admpages\models\Page */

Yii::$app->params['html.canonical'] = Url::to('', true);
$appAsset = \app\assets_b\AppAsset::register($this);

$this->context->layout = '/main';
/*
Yii::$app->params['og']['og:title'] = $this->title;
Yii::$app->params['og']['og:type'] = 'website';
Yii::$app->params['og']['og:url'] = Yii::$app->params['html.canonical'];
Yii::$app->params['og']['og:image'] = Url::to('@web/files/banners/main_banner_' . Yii::$app->language . '.jpg', true);
Yii::$app->params['og']['og:description'] = $model->description;
*/

$categories = \app\models\Category::getCategories();

$print = Yii::$app->request->get('print');


?>

<div class="main-page container">
    <div style="margin: 10px 0px;">
        <a href="<?= Url::current(['print' => 1]) ?>" class="btn btn-primary">print</a>
        <a href="<?= Url::current(['print' => null]) ?>" class="btn btn-primary">json</a>
    </div>

    <div style="border: 1px solid black; margin-bottom: 10px;">
        <ul>
            <?php foreach ($categories as $cat) {?>
                <li>
                    <a href="<?= Url::to(['/api/news', 'cat_id' => $cat['id'], 'deep' => 3, 'print' => $print]) ?>" target="_blank">(<?= $cat['title'] ?>) <?= Url::to(['/api/news', 'cat_id' => $cat['id'], 'deep' => 3, 'print' => $print]) ?></a>
                    <?php if ($cat['children']) {?>
                        <ul>
                            <?php foreach ($cat['children'] as $sub) {?>
                                <li>
                                    <a href="<?= Url::to(['/api/news', 'cat_id' => $sub['id'], 'deep' => 3, 'print' => $print]) ?>" target="_blank">(<?= $sub['title'] ?>) <?= Url::to(['/api/news', 'cat_id' => $sub['id'], 'deep' => 3, 'print' => $print]) ?></a>
                                    <?php if ($sub['children']) {?>
                                        <ul>
                                            <?php foreach ($sub['children'] as $subsub) {?>
                                                <li>
                                                    <a href="<?= Url::to(['/api/news', 'cat_id' => $subsub['id'], 'deep' => 3, 'print' => $print]) ?>" target="_blank">(<?= $subsub['title'] ?>) <?= Url::to(['/api/news', 'cat_id' => $subsub['id'], 'deep' => 3, 'print' => $print]) ?></a>
                                                </li>
                                            <?php }?>
                                        </ul>
                                    <?php }?>
                                </li>
                            <?php }?>
                        </ul>
                    <?php }?>
                </li>
            <?php }?>
        </ul>

    </div>
    <div style="border: 1px solid black; margin-bottom: 10px;">
        <a href="<?= Url::to(['/api/category', 'deep' => 3, 'print' => $print]) ?>" target="_blank"><?= Url::to(['/api/category', 'deep' => 3, 'print' => $print]) ?></a>
    </div>
</div>
