-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 20, 2020 at 10:03 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medialine`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_parent` bigint(20) DEFAULT NULL,
  `title` varchar(100) NOT NULL COMMENT 'name',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `id_parent`, `title`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Общество', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(2, 1, 'Городская жизнь', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(3, 1, 'Выборы', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(4, NULL, 'День города', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(5, 4, 'Cалюты\r\n', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(6, 4, 'Детская площадка', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(7, 6, '0-3 года', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(8, 6, '3-7 года', '2020-12-19 11:44:35', '2020-12-19 11:44:35'),
(9, NULL, 'Спорт', '2020-12-19 11:44:35', '2020-12-19 11:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL COMMENT 'name',
  `text` text NOT NULL COMMENT 'redactor',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'created_at',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `created_at`, `updated_at`) VALUES
(1, 'News 1', 'Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi. Phasellus a est. Sed fringilla mauris sit amet nibh. Donec posuere vulputate arcu. Vivamus laoreet.\r\n\r\nMorbi nec metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam sit amet orci eget eros faucibus tincidunt. Proin faucibus arcu quis ante. Sed augue ipsum, egestas nec, vestibulum et, malesuada adipiscing, dui.\r\n\r\nIn turpis. Etiam sit amet orci eget eros faucibus tincidunt. Sed in libero ut nibh placerat accumsan. In dui magna, posuere eget, vestibulum et, tempor auctor, justo. Donec vitae sapien ut libero venenatis faucibus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(2, 'News 2', 'Maecenas nec odio et ante tincidunt tempus. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. In hac habitasse platea dictumst. In ut quam vitae odio lacinia tincidunt. Fusce pharetra convallis urna.\r\n\r\nQuisque id mi. Praesent ac sem eget est egestas volutpat. Aenean vulputate eleifend tellus. Donec posuere vulputate arcu. Proin faucibus arcu quis ante.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(3, 'News 3', 'Vestibulum eu odio. Donec mollis hendrerit risus. Nunc nec neque. Nullam cursus lacinia erat. Morbi mollis tellus ac sapien.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(4, 'News 4', 'Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Etiam sollicitudin, ipsum eu pulvinar rutrum, tellus ipsum laoreet sapien, quis venenatis ante odio sit amet eros. Nam commodo suscipit quam. Fusce a quam. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est.\r\n\r\nIn auctor lobortis lacus. Etiam ut purus mattis mauris sodales aliquam. Nullam accumsan lorem in dui. Praesent metus tellus, elementum eu, semper a, adipiscing nec, purus. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.\r\n\r\nCras dapibus. Sed aliquam ultrices mauris. Proin faucibus arcu quis ante. Nam adipiscing. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(5, 'News 5', 'Etiam feugiat lorem non metus. Duis leo. Fusce vel dui. Vestibulum suscipit nulla quis orci. Fusce vulputate eleifend sapien.\r\n\r\nVivamus aliquet elit ac nisl. Aenean imperdiet. Donec posuere vulputate arcu. Praesent egestas tristique nibh. Nullam cursus lacinia erat.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(6, 'News 6', 'Nullam tincidunt adipiscing enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Praesent nec nisl a purus blandit viverra. Phasellus ullamcorper ipsum rutrum nunc.\r\n\r\nFusce commodo aliquam arcu. Duis vel nibh at velit scelerisque suscipit. Quisque id mi. Fusce vulputate eleifend sapien. Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(7, 'News 7', 'Nullam tincidunt adipiscing enim. Nullam cursus lacinia erat. Nam commodo suscipit quam. Nam at tortor in tellus interdum sagittis. Vestibulum fringilla pede sit amet augue.\r\n\r\nFusce pharetra convallis urna. Cras dapibus. In auctor lobortis lacus. Suspendisse potenti. Phasellus tempus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(8, 'News 8', 'Praesent egestas neque eu enim. Vestibulum ullamcorper mauris at ligula. Etiam ut purus mattis mauris sodales aliquam. Cras risus ipsum, faucibus ut, ullamcorper id, varius ac, leo. Proin magna.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Aenean ut eros et nisl sagittis vestibulum. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(9, 'News 9', 'Praesent egestas tristique nibh. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Fusce convallis metus id felis luctus adipiscing. Ut tincidunt tincidunt erat. In ut quam vitae odio lacinia tincidunt.\r\n\r\nAenean vulputate eleifend tellus. Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Vestibulum dapibus nunc ac augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(10, 'News 10', 'Praesent ac sem eget est egestas volutpat. Curabitur at lacus ac velit ornare lobortis. Curabitur ullamcorper ultricies nisi. Cras dapibus. In consectetuer turpis ut velit.\r\n\r\nMaecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Etiam imperdiet imperdiet orci. Duis leo. Praesent ut ligula non mi varius sagittis. Sed hendrerit.\r\n\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Morbi mollis tellus ac sapien. Suspendisse pulvinar, augue ac venenatis condimentum, sem libero volutpat nibh, nec pellentesque velit pede quis nunc. Ut a nisl id ante tempus hendrerit.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(11, 'News 11', 'Phasellus viverra nulla ut metus varius laoreet. Vestibulum eu odio. Duis lobortis massa imperdiet quam. Phasellus ullamcorper ipsum rutrum nunc. Fusce commodo aliquam arcu.\r\n\r\nDuis vel nibh at velit scelerisque suscipit. Donec vitae sapien ut libero venenatis faucibus. Curabitur vestibulum aliquam leo. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem. Fusce fermentum odio nec arcu.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(12, 'News 12', 'Pellentesque ut neque. Quisque malesuada placerat nisl. Donec elit libero, sodales nec, volutpat a, suscipit non, turpis. Ut varius tincidunt libero. Aenean vulputate eleifend tellus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(13, 'News 13', 'Nunc nonummy metus. Ut a nisl id ante tempus hendrerit. Sed hendrerit. Curabitur turpis. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.\r\n\r\nPhasellus viverra nulla ut metus varius laoreet. Aliquam lobortis. Aenean viverra rhoncus pede. Aliquam erat volutpat. Aenean massa.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(14, 'News 14', 'Aliquam eu nunc. Duis leo. Etiam ut purus mattis mauris sodales aliquam. Nunc interdum lacus sit amet orci. Praesent ut ligula non mi varius sagittis.\r\n\r\nSed aliquam ultrices mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Fusce id purus. Aenean commodo ligula eget dolor. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Aenean vulputate eleifend tellus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(15, 'News 15', 'Donec mi odio, faucibus at, scelerisque quis, convallis in, nisi. Ut a nisl id ante tempus hendrerit. Maecenas nec odio et ante tincidunt tempus. Mauris sollicitudin fermentum libero. Sed libero.\r\n\r\nSed libero. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Nunc sed turpis. Pellentesque dapibus hendrerit tortor. Phasellus accumsan cursus velit.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(16, 'News 16', 'Nunc sed turpis. Sed mollis, eros et ultrices tempus, mauris ipsum aliquam libero, non adipiscing dolor urna a orci. Nam eget dui. Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc, eu sollicitudin urna dolor sagittis lacus. Phasellus nec sem in justo pellentesque facilisis.\r\n\r\nIn enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Aenean viverra rhoncus pede. Phasellus ullamcorper ipsum rutrum nunc. In consectetuer turpis ut velit. Proin pretium, leo ac pellentesque mollis, felis nunc ultrices eros, sed gravida augue augue mollis justo.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(17, 'News 17', 'Mauris sollicitudin fermentum libero. Vivamus aliquet elit ac nisl. Morbi mollis tellus ac sapien. Phasellus gravida semper nisi. Curabitur blandit mollis lacus.\r\n\r\nNam adipiscing. Fusce fermentum odio nec arcu. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed aliquam, nisi quis porttitor congue, elit erat euismod orci, ac placerat dolor lectus quis orci. Nullam vel sem. Aenean vulputate eleifend tellus.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(18, 'News 18', 'In hac habitasse platea dictumst. Praesent nonummy mi in odio. Suspendisse potenti. Donec orci lectus, aliquam ut, faucibus non, euismod id, nulla. Quisque rutrum.\r\n\r\nPhasellus volutpat, metus eget egestas mollis, lacus lacus blandit dui, id egestas quam mauris ut lacus. Vivamus aliquet elit ac nisl. Aenean vulputate eleifend tellus. Vestibulum rutrum, mi nec elementum vehicula, eros quam gravida nisl, id fringilla neque ante vel mi. Etiam sit amet orci eget eros faucibus tincidunt.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(19, 'News 19', 'Praesent nec nisl a purus blandit viverra. Vestibulum volutpat pretium libero. Praesent nec nisl a purus blandit viverra. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Sed aliquam ultrices mauris.\r\n\r\nDonec mollis hendrerit risus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus magna. Praesent ut ligula non mi varius sagittis. Ut non enim eleifend felis pretium feugiat.', '2020-12-19 12:31:19', '2020-12-19 12:31:19'),
(20, 'News 20', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec mollis hendrerit risus. Morbi ac felis. Praesent vestibulum dapibus nibh. Nulla neque dolor, sagittis eget, iaculis quis, molestie non, velit.\r\n\r\nIn hac habitasse platea dictumst. Morbi nec metus. Phasellus ullamcorper ipsum rutrum nunc. Curabitur turpis. Morbi mattis ullamcorper velit.', '2020-12-19 12:31:19', '2020-12-19 12:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

DROP TABLE IF EXISTS `news_category`;
CREATE TABLE IF NOT EXISTS `news_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `news_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'updated_at',
  PRIMARY KEY (`id`),
  KEY `news_id` (`news_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_category`
--

INSERT INTO `news_category` (`id`, `news_id`, `category_id`, `updated_at`) VALUES
(21, 1, 1, '2020-12-19 14:26:10'),
(22, 2, 2, '2020-12-19 14:26:10'),
(23, 3, 3, '2020-12-19 14:26:10'),
(24, 4, 4, '2020-12-19 14:26:10'),
(25, 5, 5, '2020-12-19 14:26:10'),
(26, 6, 6, '2020-12-19 14:26:10'),
(27, 7, 7, '2020-12-19 14:26:10'),
(28, 8, 8, '2020-12-19 14:26:10'),
(29, 9, 9, '2020-12-19 14:26:10'),
(30, 10, 3, '2020-12-19 14:26:10'),
(31, 11, 5, '2020-12-19 14:26:10'),
(32, 12, 8, '2020-12-19 14:26:10'),
(33, 13, 2, '2020-12-19 14:26:10'),
(34, 14, 1, '2020-12-19 14:26:10'),
(35, 15, 8, '2020-12-19 14:26:10'),
(36, 16, 9, '2020-12-19 14:26:10'),
(37, 17, 7, '2020-12-19 14:26:10'),
(38, 18, 4, '2020-12-19 14:26:10'),
(39, 19, 4, '2020-12-19 14:26:10'),
(40, 20, 3, '2020-12-19 14:26:10');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`id_parent`) REFERENCES `category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `news_category`
--
ALTER TABLE `news_category`
  ADD CONSTRAINT `news_category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_category_ibfk_2` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
