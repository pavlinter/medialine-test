<?php

use yii\db\Migration;

/**
 * Class m201220_102852_medialine
 */
class m201220_102852_medialine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql = file_get_contents(__DIR__ . '/medialine.sql');
        Yii::$app->db->pdo->exec($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%news_category}}');
        $this->dropTable('{{%category}}');
        $this->dropTable('{{%news}}');
    }
}
